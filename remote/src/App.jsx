import { render } from "solid-js/web";

import SDK from "./SDK";

import "./index.scss";

const App = () => (
  <div class="mt-10 text-3xl mx-auto max-w-6xl">
    <div>Name: SDK Project</div>
    <SDK />
  </div>
);

render(App, document.getElementById("app"));
