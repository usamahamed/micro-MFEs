import { createSignal } from "solid-js";

export default () => {
    const [componentId, setComponentId] = createSignal('https://test.roomle.com/t/cp/?configuratorId=vizmotor&id=strasser:variation-sofa145654g2g23&api=false');
    const onEnterComponentId = event => {
        return setComponentId('https://test.roomle.com/t/cp/?configuratorId=vizmotor&id=' + event.target.value + "&api=false");
      };
    return (
    // adidas:F17490  
        <div class="">
            <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2" for="component">
                Enter ID
            </label>            
            <input onChange={onEnterComponentId} class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500" id="component" type="text" placeholder="Enter component ID" />
            <div class='mt-4'></div>
            <iframe src={componentId()} width="1154" height="768"></iframe>
    </div>
  );
};
