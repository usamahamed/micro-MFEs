import { render } from "solid-js/web";

import Counter from "./SDK";

export default (el) => {
  render(Counter, el);
};
